package ru.pflb.autotest.stackoverflow.pages;

import org.openqa.selenium.WebElement;

public class MainPage extends BasePage{
    public void open(){
        driver.get("http://www.stackoverflow.com");
    }
    public void setSearchField(String searchQuery){
        String searchFieldXpath = "//input[@name='q111']";
        WebElement searchField = driver.findElementByXPath(searchFieldXpath);
        searchField.sendKeys(searchQuery);
    }
    public void clickSearchButton(){
        String searchButtonXpath = "//button[@aria-label='Search…']";
        WebElement searchButton = driver.findElementByXPath(searchButtonXpath);
        searchButton.click();
    }
}
